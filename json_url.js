define(function(){
	var JSON_URL = {
		toJSON : function(url){
			url = url.split('?');
			url = url[url.length-1];
			
			var json = {};
			url = url.split('&');
			for(var i=0; i<url.length; i++){
				json[url[i].split('=')[0]] = url[i].split('=')[1];
			}

			return json;
		},
		toURL : function(route, parameters){
			var target = route;
			if(target.length > 0) target += '?';

			for(var key in parameters) target += key + '=' + parameters[key] + '&';

			if(target.length > 0) target = target.substr(0, target.length - 1);

			return target;
		}
	};

	return JSON_URL;
});